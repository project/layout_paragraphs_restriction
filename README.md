# Layout Paragraphs Restriction

## Overview
Layout Paragraphs Restriction is an extension of the Layout Paragraphs module, which adds the functionality to restrict available paragraphs for a specific layout. The module is a rewrite of the ERL Restriction module, which provides the almost same functionality for the Entity Reference with Layouts module.

## Usage
After installation, go to your Content Type and click on "Manage Form Display".
On your Entity reference revisions (paragraphs) field change the field widget to "Layout Paragraphs Restriction" and open the widget settings.
Now you can define the paragraphs that will be allowed for each section in each of the enabled layouts.

## Example
Imagine you have a two column layout with one column using 67% of the width and the other one 33%. Now you could define, that an image gallery will be allowed in the larger column, but disallowed in the smaller column.
After you configured that, an editor will only be able to add the image gallery in the 67% column, but not in the 33% column.
