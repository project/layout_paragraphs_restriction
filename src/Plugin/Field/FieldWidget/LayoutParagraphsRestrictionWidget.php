<?php

namespace Drupal\layout_paragraphs_restriction\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\layout_paragraphs\Plugin\Field\FieldWidget\LayoutParagraphsWidget;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity Reference with Layout field widget.
 *
 * @FieldWidget(
 *   id = "layout_paragraphs_restriction_widget",
 *   label = @Translation("Layout Paragraphs Restriction"),
 *   description = @Translation("Layout builder for paragraphs."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   },
 * )
 */
class LayoutParagraphsRestrictionWidget extends LayoutParagraphsWidget implements ContainerFactoryPluginInterface {

  /**
   * All configured paragraphs are allowed.
   */
  const BLACKLIST_MODE_INCLUDE = 'include';
  /**
   * All configured paragraphs are disallowed.
   */
  const BLACKLIST_MODE_EXCLUDE = 'exclude';


  protected $paragraphInfo;

  protected $restrictions;

  protected $fieldSettings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->paragraphInfo = $instance->entityTypeBundleInfo->getBundleInfo('paragraph');
    $instance->restrictions = $instance->getSetting('restrictions');
    $instance->fieldSettings = $instance->fieldDefinition->getSetting('handler_settings');
    return $instance;
  }

  /**
   * Builds the main widget form array container/wrapper.
   *
   * Form elements for individual items are built by formElement().
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);

    $parents = $form['#parents'];
    $widget_state = static::getWidgetState($parents, $this->fieldName, $form_state);
    if ($widget_state['items']) {
      foreach ($widget_state['items'] as $delta => $item) {
        if (!isset($item['entity'])) {
          continue;
        }
        // Check if the paragraph has an toggle button.
        if (isset($elements[$delta]['toggle_button'])) {

        }
      }
    }


    // Replace the library.
    if (FALSE !== $key = array_search('layout_paragraphs/layout_paragraphs_widget', $elements['#attached']['library'])) {
      $elements['#attached']['library'][$key] = 'layout_paragraphs_restriction/layout_paragraphs_restriction_widget';
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (isset($element['#entity']) && $element['#entity'] instanceof Paragraph) {
      $paragraphType = $element['#entity']->bundle();
      // Add the paragraph type as data attribute.
      if (isset($element['#attributes']['class']) && in_array('layout-paragraphs-item', $element['#attributes']['class'])) {
        $element['#attributes']['data-paragraph-type'] = $paragraphType;
      }
      if (!empty($element['#layout']) && !empty($element['preview']['regions'])) {
        foreach ($element['preview']['regions'] as $regionId => $region) {
          $allowedTypes = $this->getAllowedParagraphTypes($paragraphType, $element['#layout'], $regionId);
          $element['preview']['regions'][$regionId]['#attributes']['data-allowed-paragraph-types'] = json_encode(array_keys($allowedTypes));
        }
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaults = parent::defaultSettings();
    $defaults += [
      'restrictions' => [
        'blacklist' => [],
      ],
    ];

    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    // Get all possible paragraphs machine names and labels.
    $options = $this->getParagraphLabelsFromKeys($this->getTargetBundlesWithoutLayoutBundles());

    $form['restrictions'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure restrictions'),
    ];

    // Add a redio element to decide wether all configured paragraph types are
    // in- or excluded.
    $form['restrictions']['blacklist'] = [
      '#type' => 'radios',
      '#title' => $this->t('Which paragraph types should be allowed?'),
      '#options' => [
        self::BLACKLIST_MODE_INCLUDE => $this->t('Include the selected below'),
        self::BLACKLIST_MODE_EXCLUDE => $this->t('Exclude the selected below'),
      ],
      '#default_value' => isset($this->getSetting('restrictions')['blacklist']) ? $this->getSetting('restrictions')['blacklist'] : self::BLACKLIST_MODE_INCLUDE,
    ];
    // Get all possible layout bundles.
    $layoutBundles = $this->getLayoutBundles();
    // Get all layout definitions.
    $definitions = $this->layoutPluginManager->getDefinitions();
    // Initiate the paragraphs storage service.
    $paragraphs_type_storage = $this->entityTypeManager->getStorage('paragraphs_type');
    foreach ($layoutBundles as $layoutBundle) {
      // Get the allowed layouts for this layout paragraph.
      $allowedLayouts = $this->getAllowedLayouts($layoutBundle);
      /** @var \Drupal\paragraphs\Entity\ParagraphsType $paragraph_type */
      $paragraph_type = $paragraphs_type_storage->load($layoutBundle);
      $form['restrictions'][$layoutBundle] = [
        '#type' => 'details',
        '#title' => $paragraph_type->label(),
      ];
      // Loop over all allowed layouts of this layout paragraph type.
      foreach ($allowedLayouts as $allowed_layout_type => $allowed_layout_name) {
        $form['restrictions'][$layoutBundle][$allowed_layout_type] = [
          '#type' => 'details',
          '#title' => $allowed_layout_name,
        ];
        // If we don't have a definition for this layout type: Skip and
        // continue.
        if (empty($definitions[$allowed_layout_type])) {
          continue;
        }
        // Loop over all regions of this layout and add a checkboxes element
        // with all possible paragraph types as options.
        foreach ($definitions[$allowed_layout_type]->getRegions() as $region_key => $region) {
          $form['restrictions'][$layoutBundle][$allowed_layout_type][$region_key] = [
            '#type' => 'checkboxes',
            '#title' => $region['label'],
            '#options' => $options,
            '#default_value' => isset($this->restrictions[$layoutBundle][$allowed_layout_type][$region_key]) ? $this->restrictions[$layoutBundle][$allowed_layout_type][$region_key] : [],
          ];
        }
      }
    }

    return $form;
  }

  /**
   * Return all targets bundles without the layout bundles.
   *
   * @return array|mixed
   *   Returns the target bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTargetBundlesWithoutLayoutBundles() {
    $targetBundles = $this->fieldSettings['target_bundles'];
    $paragraphs_type_storage = $this->entityTypeManager->getStorage('paragraphs_type');

    foreach ($targetBundles as $key => $targetBundle) {
      /** @var \Drupal\paragraphs\Entity\ParagraphsType $paragraphs_type */
      $paragraphs_type = $paragraphs_type_storage->load($targetBundle);
      $layout_paragraphs_behavior = $paragraphs_type->getBehaviorPlugin('layout_paragraphs');
      $layout_paragraphs_behavior_config = $layout_paragraphs_behavior->getConfiguration();

      if (!empty($layout_paragraphs_behavior_config['enabled'])) {
        unset($targetBundles[$key]);
      }
    }
    return is_null($targetBundles) ? [] : $targetBundles;
  }

  /**
   * Get all layout bundles from the list of target bundles configured.
   *
   * @return array|mixed
   *   The layout bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getLayoutBundles() {
    // Get the target bundles configured for the field.
    $targetBundles = $this->fieldSettings['target_bundles'];
    $paragraphs_type_storage = $this->entityTypeManager->getStorage('paragraphs_type');

    // Loop over all the target bundles.
    foreach ($targetBundles as $key => $targetBundle) {
      // Load the paragraph type.
      /** @var \Drupal\paragraphs\Entity\ParagraphsType $paragraphs_type */
      $paragraphs_type = $paragraphs_type_storage->load($targetBundle);
      // Check if we have a layout paragraphs behavior plugin set for the
      // paragraph type.
      $layout_paragraphs_behavior = $paragraphs_type->getBehaviorPlugin('layout_paragraphs');
      $layout_paragraphs_behavior_config = $layout_paragraphs_behavior->getConfiguration();
      // If the behavior is not active, remove the paragraph bundle from the
      // possible layout bundles array.
      if (empty($layout_paragraphs_behavior_config['enabled'])) {
        unset($targetBundles[$key]);
      }
    }
    return is_null($targetBundles) ? [] : $targetBundles;
  }

  /**
   * Returns the allowed layouts configured for a layout paragraph.
   *
   * @param string $bundle
   *   The bundle of the paragraph type.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllowedLayouts($bundle) {
    $allowedLayouts = [];
    $paragraphs_type_storage = $this->entityTypeManager->getStorage('paragraphs_type');
    $paragraphs_type = $paragraphs_type_storage->load($bundle);
    if (empty($paragraphs_type)) {
      return [];
    }
    $layout_paragraphs_behavior = $paragraphs_type->getBehaviorPlugin('layout_paragraphs');
    if (empty($layout_paragraphs_behavior)) {
      return [];
    }
    $layout_paragraphs_behavior_config = $layout_paragraphs_behavior->getConfiguration();
    if (!empty($layout_paragraphs_behavior_config['available_layouts'])) {
      foreach ($layout_paragraphs_behavior_config['available_layouts'] as $layoutKey => $layoutName) {
        $allowedLayouts[$layoutKey] = $layoutName;
      }
    }
    return $allowedLayouts;
  }

  /**
   * Returns the allowed paragraph types inside a region of a paragraphs layout.
   *
   * @param string $bundle
   *   The layout paragraph bundle.
   * @param string $layout
   *   The machine name of the layout.
   * @param string $region
   *   The name of the region inside the layout.
   *
   * @return array|mixed
   *   The possble paragraphs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllowedParagraphTypes($bundle, $layout, $region) {
    $paragraphs_type_storage = $this->entityTypeManager->getStorage('paragraphs_type');
    $paragraph_type = $paragraphs_type_storage->load($bundle);
    $layout_paragraphs_behavior = $paragraph_type->getBehaviorPlugin('layout_paragraphs');
    $layout_paragraphs_behavior_config = $layout_paragraphs_behavior->getConfiguration();
    if (!empty($layout_paragraphs_behavior_config['available_layouts'][$layout])) {
      // Which bundle can be referenced due to the fields settings.
      $targetBundles = $this->fieldSettings['target_bundles'];
      $blacklistMode = isset($this->restrictions['blacklist']) ? $this->restrictions['blacklist'] : self::BLACKLIST_MODE_INCLUDE;
      // If no restrictions are set here, we allow all.
      if (empty(array_filter($this->restrictions[$bundle][$layout][$region]))) {
        return $targetBundles;
      }
      // If we have restrictions set and the blacklist mode is set to include.
      elseif ($blacklistMode == self::BLACKLIST_MODE_INCLUDE) {
        $allowed = $this->restrictions[$bundle][$layout][$region];
        // Use the intersection to find the allowed paragraphs.
        $intersection = array_intersect($targetBundles, $allowed);
        return $intersection;
      }
      // If we have restrictions set and the blacklist mode is set to exclude.
      elseif ($blacklistMode == self::BLACKLIST_MODE_EXCLUDE) {
        $disallowed = array_filter($this->restrictions[$bundle][$layout][$region]);
        // Use the diff to find the allowed paragraphs.
        $diff = array_diff($targetBundles, $this->restrictions[$bundle][$layout][$region]);
        return $diff;
      }
    }
    return [];
  }

  /**
   * Gets the paragraph label for an array of paragraph keys and creates a
   * key=>label array.
   *
   * @param array $keys
   *
   * @return array
   */
  protected function getParagraphLabelsFromKeys(array $keys) {
    $withLabel = [];
    foreach ($keys as $key) {
      if (isset($this->paragraphInfo[$key]['label'])) {
        $withLabel[$key] = $this->paragraphInfo[$key]['label'];
      }
    }
    return $withLabel;
  }
}
